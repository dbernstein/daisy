jQuery(function () {
  jQuery("#filter-by-mobile, .sidebar-mobile-close").on("click", function () {
    jQuery(".col-widgets-sidebar").toggleClass("active");
    jQuery(".sections-container").toggleClass("active-sp-filter");
  });

  jQuery(document).click(function (event) {
    if (jQuery('.col-widgets-sidebar').hasClass("active") && event.pageX > 300) {
      jQuery(".col-widgets-sidebar").toggleClass("active");
      jQuery(".sections-container").toggleClass("active-sp-filter");
    }
  });


  jQuery(document).on('facetwp-loaded', function () {
    if (FWP.loaded) {
      jQuery('html, body').animate({
        scrollTop: jQuery('.facetwp-template').offset().top - 250
      }, 500);
    }
  });

  // Start Tiny sliders
  // Slider for recipe detail carousel 
  if (jQuery(".my-slider").length) {
    tns({
      container: ".my-slider",
      items: 1,
      navContainer: "#customize-thumbnails",
      navAsThumbnails: true,
      autoplayTimeout: 1000,
      swipeAngle: false,
      speed: 400,
      controls: false
    });
  }
  // Slider for recipe detail recommendations
  if (jQuery(".rp4wp-posts-list").length) {
    tns({
      container: ".rp4wp-posts-list",
      items: 1,
      responsive: {
        "500": {
          items: 2
        }
      },
      responsive: {
        "950": {
          items: 3
        }
      },
      autoplayTimeout: 1000,
      mouseDrag: true,
      speed: 400,
      nav: true,
      controls: false,
      navPosition: "bottom"
    });
  }
  // End Tiny SLiders

  // Start Tooltips
  jQuery("a[data-tooltip]").on("click", function (event) {
    event.preventDefault();
  });
  // End Tooltips

  // Start search results page hide tabs
  jQuery(".page-type-facet-wrapper .facetwp-radio").on("click", function () {
    let el = jQuery(this);
    console.log(el);
    if (el.attr("data-value") == "recipe") {
      jQuery(".page-body .un-sidebar-layout .row-inner > .col-lg-4").show();
    } else if (el.hasClass("checked")) {
      jQuery(".page-body .un-sidebar-layout .row-inner > .col-lg-4").show();
    } else {
      jQuery(".page-body .un-sidebar-layout .row-inner > .col-lg-4").hide();
    }
  });

  // Start object fit polyfill
  var ua = window.navigator.userAgent;
  var trident = ua.indexOf("Trident/");
  if (trident > 0) {
    jQuery(
      ".fwpl-result .fwpl-item, .search-results article.type-recipe .column, #customize-thumbnails li, .rp4wp_component_image"
    ).each(function () {
      var $container = jQuery(this),
        imgUrl = $container.find("img").prop("src");
      if (imgUrl) {
        $container
          .css("backgroundImage", "url(" + imgUrl + ")")
          .addClass("compat-object-fit");
      }
    });
  }
  // On facetwp ajax refresh do polyfill again
  jQuery(document).on("facetwp-loaded", function () {
    jQuery(
      ".fwpl-result .fwpl-item, .search-results article.type-recipe .column"
    ).each(function () {
      var $container = jQuery(this),
        imgUrl = $container.find("img").prop("src");
      if (imgUrl) {
        $container
          .css("backgroundImage", "url(" + imgUrl + ")")
          .addClass("compat-object-fit");
      }
    });
    // On refresh also check to hide widgets
    if (jQuery(".facetwp-radio[data-value='page']").hasClass("checked")) {
      jQuery(".sp-search-sidebar, .recipe-filter-wrapper").addClass("page-facet-active");
    } else {
      jQuery(".sp-search-sidebar, .recipe-filter-wrapper").removeClass("page-facet-active");
    }

  });
  // End object fit polyfill
});

// Cookie Banner
if (localStorage.getItem("cookieBannerViewed") != "viewed") {
  jQuery(".cookie-banner").delay(2000).fadeIn();
};
jQuery(".cookie-banner__close, .cookie-banner__accept").click(function () {
  jQuery(".cookie-banner").fadeOut();
  localStorage.setItem("cookieBannerViewed", "viewed");
});

jQuery(".basketfull-close, .basketfull-container").click(function () {
  jQuery(".basketfull-container").fadeOut();
});

jQuery("#shop_now_button, #menu-main-menu a[title='Shop Daisy'], #menu-main-menu-spanish a[title='Compra Daisy']").click(function (event) {
  event.preventDefault();

  var iframeElem = jQuery('#shopIframe');
  if(iframeElem.length > 0) {
    for ( var i = 0; i < iframeElem.length; i++ ) {
      if(iframeElem[i].getAttribute('data-src')) {
        iframeElem[i].setAttribute('src', iframeElem[i].getAttribute('data-src'));
      } 
    } 
  }
  
  jQuery(".basketfull-container").fadeIn();
  
});