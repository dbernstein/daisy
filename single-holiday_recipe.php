<?php

/**
 * The Template for displaying all single posts.
 *
 * @package uncode
 */

get_header();

/**
 * DATA COLLECTION - START
 *
 */

/** Init variables **/
$limit_width = $limit_content_width = $the_content = $main_content = $layout = $bg_color = $sidebar_style = $sidebar_bg_color = $sidebar = $sidebar_size = $sidebar_sticky = $sidebar_padding = $sidebar_inner_padding = $sidebar_content = $title_content = $media_content = $navigation_content = $page_custom_width = $row_classes = $main_classes = $footer_content = $footer_classes = $content_after_body = '';
$with_builder = false;

$post_type = $post->post_type;

/** Get general datas **/
if (isset($metabox_data['_uncode_specific_style'][0]) && $metabox_data['_uncode_specific_style'][0] !== '') {
	$style = $metabox_data['_uncode_specific_style'][0];
	if (isset($metabox_data['_uncode_specific_bg_color'][0]) && $metabox_data['_uncode_specific_bg_color'][0] !== '') {
		$bg_color = $metabox_data['_uncode_specific_bg_color'][0];
	}
} else {
	$style = ot_get_option('_uncode_general_style');
	if (isset($metabox_data['_uncode_specific_bg_color'][0]) && $metabox_data['_uncode_specific_bg_color'][0] !== '') {
		$bg_color = $metabox_data['_uncode_specific_bg_color'][0];
	} else {
		$bg_color = ot_get_option('_uncode_general_bg_color');
	}
}
$bg_color = ($bg_color == '') ? ' style-' . $style . '-bg' : ' style-' . $bg_color . '-bg';


/** Get page width info **/
$boxed = ot_get_option('_uncode_boxed');

$page_content_full = (isset($metabox_data['_uncode_specific_layout_width'][0])) ? $metabox_data['_uncode_specific_layout_width'][0] : '';
if ($page_content_full === '') {

	/** Use generic page width **/
	$generic_content_full = ot_get_option('_uncode_' . $post_type . '_layout_width');
	if ($generic_content_full === '') {
		$main_content_full = ot_get_option('_uncode_body_full');
		if ($main_content_full === '' || $main_content_full === 'off') {
			$limit_content_width = ' limit-width';
		}
	} else {
		if ($generic_content_full === 'limit') {
			$generic_custom_width = ot_get_option('_uncode_' . $post_type . '_layout_width_custom');
			if ($generic_custom_width[1] === 'px') {
				$generic_custom_width[0] = 12 * round(($generic_custom_width[0]) / 12);
			}
			if (is_array($generic_custom_width) && !empty($generic_custom_width)) {
				$page_custom_width = ' style="max-width: ' . implode("", $generic_custom_width) . '; margin: auto;"';
			}
		}
	}
} else {

	/** Override page width **/
	if ($page_content_full === 'limit') {
		$limit_content_width = ' limit-width';
		$page_custom_width = (isset($metabox_data['_uncode_specific_layout_width_custom'][0])) ? unserialize($metabox_data['_uncode_specific_layout_width_custom'][0]) : '';
		if (is_array($page_custom_width) && !empty($page_custom_width) && $page_custom_width[0] !== '') {
			if ($page_custom_width[1] === 'px') {
				$page_custom_width[0] = 12 * round(($page_custom_width[0]) / 12);
			}
			$page_custom_width = ' style="max-width: ' . implode("", $page_custom_width) . '; margin: auto;"';
		} else {
			$page_custom_width = '';
		}
	}
}

$media = get_post_meta($post->ID, '_uncode_featured_media', 1);
$media_display = get_post_meta($post->ID, '_uncode_featured_media_display', 1);
$featured_image = get_post_thumbnail_id($post->ID);
if ($featured_image === '') {
	$featured_image = $media;
}

/** Collect header data **/
if (isset($metabox_data['_uncode_header_type'][0]) && $metabox_data['_uncode_header_type'][0] !== '') {
	$page_header_type = $metabox_data['_uncode_header_type'][0];
	if ($page_header_type !== 'none') {
		$meta_data = uncode_get_specific_header_data($metabox_data, $post_type, $featured_image);
		$metabox_data = $meta_data['meta'];
		$show_title = $meta_data['show_title'];
	}
} else {
	$page_header_type = ot_get_option('_uncode_' . $post_type . '_header');
	if ($page_header_type !== '' && $page_header_type !== 'none') {
		$metabox_data['_uncode_header_type'] = array($page_header_type);
		$meta_data = uncode_get_general_header_data($metabox_data, $post_type, $featured_image);
		$metabox_data = $meta_data['meta'];
		$show_title = $meta_data['show_title'];
	}
}

/** Get layout info **/
if (isset($metabox_data['_uncode_active_sidebar'][0]) && $metabox_data['_uncode_active_sidebar'][0] !== '') {
	if ($metabox_data['_uncode_active_sidebar'][0] !== 'off') {
		$layout = (isset($metabox_data['_uncode_sidebar_position'][0])) ? $metabox_data['_uncode_sidebar_position'][0] : '';
		$sidebar = (isset($metabox_data['_uncode_sidebar'][0])) ? $metabox_data['_uncode_sidebar'][0] : '';
		$sidebar_size = (isset($metabox_data['_uncode_sidebar_size'][0])) ? $metabox_data['_uncode_sidebar_size'][0] : 4;
		$sidebar_sticky = (isset($metabox_data['_uncode_sidebar_sticky'][0]) && $metabox_data['_uncode_sidebar_sticky'][0] === 'on') ? ' sticky-element sticky-sidebar' : '';
		$sidebar_fill = (isset($metabox_data['_uncode_sidebar_fill'][0])) ? $metabox_data['_uncode_sidebar_fill'][0] : '';
		$sidebar_style = (isset($metabox_data['_uncode_sidebar_style'][0])) ? $metabox_data['_uncode_sidebar_style'][0] : $style;
		$sidebar_bg_color = (isset($metabox_data['_uncode_sidebar_bgcolor'][0]) && $metabox_data['_uncode_sidebar_bgcolor'][0] !== '') ? ' style-' . $metabox_data['_uncode_sidebar_bgcolor'][0] . '-bg' : '';
	}
} else {
	$activate_sidebar = ot_get_option('_uncode_' . $post_type . '_activate_sidebar');
	$sidebar_name     = ot_get_option('_uncode_' . $post_type . '_sidebar');

	if ($activate_sidebar !== 'off' && is_active_sidebar($sidebar_name)) {
		$layout = ot_get_option('_uncode_' . $post_type . '_sidebar_position');
		if ($layout === '') {
			$layout = 'sidebar_right';
		}
		$sidebar = ot_get_option('_uncode_' . $post_type . '_sidebar');
		$sidebar_style = ot_get_option('_uncode_' . $post_type . '_sidebar_style');
		$sidebar_size = ot_get_option('_uncode_' . $post_type . '_sidebar_size');
		$sidebar_sticky = ot_get_option('_uncode_' . $post_type . '_sidebar_sticky');
		$sidebar_sticky = ($sidebar_sticky === 'on') ? ' sticky-element sticky-sidebar' : '';
		$sidebar_fill = ot_get_option('_uncode_' . $post_type . '_sidebar_fill');
		$sidebar_bg_color = ot_get_option('_uncode_' . $post_type . '_sidebar_bgcolor');
		$sidebar_bg_color = ($sidebar_bg_color !== '') ? ' style-' . $sidebar_bg_color . '-bg' : '';
	}
}
if ($sidebar_style === '') {
	$sidebar_style = $style;
}

/** Get title info **/
$generic_show_title = ot_get_option('_uncode_' . $post_type . '_title');
$page_show_title = (isset($metabox_data['_uncode_specific_title'][0])) ? $metabox_data['_uncode_specific_title'][0] : '';
if ($page_show_title === '') {
	$show_title = ($generic_show_title === 'off') ? false : true;
} else {
	$show_title = ($page_show_title === 'off') ? false : true;
}

/** Get media info **/
$generic_show_media = ot_get_option('_uncode_' . $post_type . '_media');
$page_show_media = (isset($metabox_data['_uncode_specific_media'][0])) ? $metabox_data['_uncode_specific_media'][0] : '';
if ($page_show_media === '') {
	$show_media = ($generic_show_media === 'off') ? false : true;
} else {
	$show_media = ($page_show_media === 'off') ? false : true;
}

if (!$show_media && $featured_image !== '') {
	$generic_show_featured_media = ot_get_option('_uncode_' . $post_type . '_featured_media');
	$page_show_featured_media = (isset($metabox_data['_uncode_specific_featured_media'][0]) && $metabox_data['_uncode_specific_featured_media'][0] !== '') ? $metabox_data['_uncode_specific_featured_media'][0] : $generic_show_featured_media;

	if ($page_show_featured_media === 'on') {
		$media = $featured_image;
	}
} else {
	$page_show_featured_media = false;
}

$show_media = $page_show_featured_media && $page_show_featured_media !== 'off' ? true : $show_media;

/**
 * DATA COLLECTION - END
 *
 */

while (have_posts()) :
	the_post();

	/** Build header **/
	if ($page_header_type !== '' && $page_header_type !== 'none') {
		$page_header = new unheader($metabox_data, $post->post_title, $post->post_excerpt);

		$header_html = $page_header->html;
		if ($header_html !== '') {
			echo '<div id="page-header">';
			echo uncode_remove_p_tag($page_header->html);
			echo '</div>';
		}

		if (!empty($page_header->poster_id) && $page_header->poster_id !== false && $media !== '') {
			$media = $page_header->poster_id;
		}
	}
	echo '<script type="text/javascript">UNCODE.initHeader();</script>';

	/** Build media **/

	if ($media !== '' && !$with_builder && $show_media && !post_password_required()) {
		if ($layout === 'sidebar_right' || $layout === 'sidebar_left') {
			$media_size = 12 - $sidebar_size;
		} else {
			$media_size = 12;
		}

		$media_array = explode(',', $media);
		$media_counter = count($media_array);
		$rand_id = uncode_big_rand();
		if ($media_counter === 0) {
			$media_array = array($media);
		}

		if ($media_display === 'isotope') {
			$media_content .= '<div id="gallery-' . $rand_id . '" class="isotope-system post-media">
				<div class="isotope-wrapper half-gutter">
	      	<div class="isotope-container isotope-layout style-masonry" data-type="masonry" data-layout="masonry" data-lg="1000" data-md="600" data-sm="480">';
		}

		foreach ($media_array as $key => $value) { //check if albums are set among medias
			if (get_post_mime_type($value) == 'oembed/gallery' && wp_get_post_parent_id($value)) {
				$parent_id = wp_get_post_parent_id($value);
				$media_album_ids = get_post_meta($parent_id, '_uncode_featured_media', true);
				$media_arr = explode(',', $media); //eplode $media string to add album IDs
				$media_album_ids_arr = explode(',', $media_album_ids);
				if (is_array($media_album_ids_arr) && !empty($media_album_ids_arr)) {
					unset($media_array[$key]); //remove album featured image from array
					$media_album_ids_arr = array_reverse($media_album_ids_arr);
					foreach ($media_album_ids_arr as $_key => $_value) {
						array_splice($media_array, $key, 0, $_value);
						array_splice($media_arr, $key, 0, $_value);
					}
				}
				$media = implode(",", $media_arr); //implode $media again after adding album IDs
			}
		}

		foreach ($media_array as $key => $value) {
			if ($media_display === 'carousel') {
				$value = $media;
			}
			$block_data = array();
			$block_data['media_id'] = $value;
			$block_data['classes'] = array(
				'tmb'
			);
			$block_data['text_padding'] = 'no-block-padding';
			if ($media_display === 'isotope') {
				$block_data['single_width'] = 4;
				$block_data['classes'][] = 'tmb-iso-w4';
			} else {
				$block_data['single_width'] = $media_size;
			}
			$block_data['single_style'] = $style;
			$block_data['single_text'] = 'under';
			$block_data['classes'][] = 'tmb-' . $style;
			if ($media_display === 'isotope') {
				$block_data['classes'][] = 'tmb-overlay-anim';
				$block_data['classes'][] = 'tmb-overlay-text-anim';
				$block_data['single_icon'] = 'fa fa-plus2';
				$block_data['overlay_color'] = ($style == 'light') ? 'style-black-bg' : 'style-white-bg';
				$block_data['overlay_opacity'] = '20';
				$lightbox_classes = array();
				$lightbox_classes['data-noarr'] = false;
			} else {
				$lightbox_classes = false;
				$block_data['link_class'] = 'inactive-link';
				$block_data['link'] = '#';
			}
			$block_data['title_classes'] = array();
			$block_data['tmb_data'] = array();
			$block_layout['media'] = array();
			$block_layout['icon'] = array();
			$media_html = uncode_create_single_block($block_data, $rand_id, 'masonry', $block_layout, $lightbox_classes, false, true);
			if ($media_display !== 'isotope') {
				$media_content .= '<div class="post-media">' . $media_html . '</div>';
			} else {
				$media_content .= $media_html;
			}
			if ($media_display === 'carousel') {
				break;
			}
		}

		if ($media_display === 'isotope') {
			$media_content .= '</div>
				</div>
			</div>';
		}
	}

	/** Build title **/

	if ($show_title) {
		$title_content .= apply_filters('uncode_before_body_title', '');
		$title_content .= '<div class="post-title-wrapper"><h1 class="post-title">' . get_the_title() . '</h1>';
		$title_content .= uncode_post_info() . '</div>';
		$title_content .= apply_filters('uncode_after_body_title', '');
	}


	/** Build Recipe Data  **/
	$my_current_lang = apply_filters('wpml_current_language', NULL);
	// set es or en words 
	if ($my_current_lang == "es") {
		$prep_time_word = 'Preparación';
		$time_table_word = 'Tiempo Total';
		$serves_word = 'Rinde';
		$print_recipe_word = 'Imprimir Receta';
		$shop_now_word = 'Compra Daisy';
		$save_now_word = 'Ahorrar Ahora';
		$share_word = 'Compartir';
		$ingredient_direction_word = 'Ingredientes e Instrucciones';
		$nutrition_info_word = 'Información Nutricional';
		$ingredients_word = 'Ingredientes';
		$directions_word = 'Instrucciones';
		$recomendations_word = 'Recomendaciones';
		$tips_word = 'Consejos';
		$shop_now_aria = 'Haga clic para comprar en Daisy.com';
	} else {
		$prep_time_word = 'Prep Time';
		$time_table_word = 'Time to Table';
		$serves_word = 'Servings';
		$print_recipe_word = 'print recipe';
		$shop_now_word = 'shop Daisy';
		$save_now_word = 'save now';
		$share_word = 'Share';
		$ingredient_direction_word = 'Ingredients & Directions';
		$nutrition_info_word = 'Nutrition Information';
		$ingredients_word = 'Ingredients';
		$directions_word = 'Directions';
		$recomendations_word = 'Recommendations';
		$tips_word = 'Tips';
		$shop_now_aria = 'Click to shop on Daisy.com';
	}

	$recipe_name = get_field('recipe_name');
	$recipe_image_id = get_post_thumbnail_id();
	$recipe_thumbnail_url = wp_get_attachment_url($recipe_image_id);
	$recipe_uploads = wp_upload_dir();
	$recipe_thumbnail_file_path = "https://" . $_SERVER['HTTP_HOST'] . $recipe_thumbnail_url;

	$recipe_heading = '<div class="heading-text el-text"><h1 class="h2"><span>' . $recipe_name . '</span></h1></div>';
	$recipe_heading_base64 = base64_encode($recipe_heading);

	// set prep time
	$recipe_prep = '';
	$recipe_prep_hours = get_field('prep_time_hours');
	if ($recipe_prep_hours) {
		$recipe_prep = $recipe_prep_hours . 'hr ';
	}
	$recipe_prep_minutes = get_field('prep_time_minutes');
	if ($recipe_prep_minutes) {
		$recipe_prep = $recipe_prep . $recipe_prep_minutes . 'mins';
	}
	// set time to table
	$recipe_time_to_table = '';
	$recipe_time_to_table_hours = get_field('time_to_table_hours');
	if ($recipe_time_to_table_hours) {
		$recipe_time_to_table = $recipe_time_to_table_hours . 'hr ';
	}
	$recipe_time_to_table_minutes = get_field('time_to_table_minutes');
	if ($recipe_time_to_table_minutes) {
		$recipe_time_to_table = $recipe_time_to_table . $recipe_time_to_table_minutes . 'mins';
	}
	// set serves
	$recipe_serves = get_field('serves');

	// put recipe times together
	$recipe_times = '<div class="recipe-times-container">
	<div class="recipe-times-item">
		<img src="/wp-content/uploads/2019/11/prep-time-icon.svg" alt="prep time icon"/>
		<p class="recipe-times-headline">' . $prep_time_word . '</p>
		<p class="recipe-times-time">' . $recipe_prep . '</p>
	</div>
	<div class="recipe-times-item">
		<img src="/wp-content/uploads/2019/11/time-table-icon.svg" alt="time to table icon"/>
		<p class="recipe-times-headline">' . $time_table_word . '</p>
		<p class="recipe-times-time">' . $recipe_time_to_table . '</p>
	</div>
	<div class="recipe-times-item">
		<img src="/wp-content/uploads/2019/11/serves-icon.svg" alt="serves icon"/>
		<p class="recipe-times-headline">' . $serves_word . '</p>
		<p class="recipe-times-time">' . $recipe_serves . '</p>
	</div>
	</div>';
	$recipe_times_base64 = base64_encode($recipe_times);

	$shop_now_button = '<a id="shop_now_button" aria-label="' . $shop_now_aria . '" href="https://shop.basketful.co/bundle?bundle=32f2f136-076b-411a-ae13-8cf5ee30fb2b" class="btn btn-no-scale btn-xl border-width-0 btn-sp-color-red btn-icon-right" target="_blank">' . $shop_now_word . '<i class="fa fa-angle-right"></i></a>';

	$recipe_print_button = '<a href="javascript:window.print()" class="btn btn-no-scale btn-xl border-width-0 btn-sp-color-red btn-icon-right">' . $print_recipe_word . '<i class="fa fa-angle-right"></i></a>';

	$save_now_button = '';

	$recipe_save_now_link = get_field('save_now_link');
	if ($recipe_save_now_link) {
		$save_now_button = '<a href="' . $recipe_save_now_link . '"target="_blank" class="btn btn-no-scale btn-xl border-width-0 btn-sp-color-red btn-icon-right"><i class="save-now-icon"></i>' . $save_now_word . '</a>';
	}

	$cta_btn_group = '<div class="btn-group">' . $shop_now_button . $recipe_print_button . $save_now_button . '</div>';

	$recipe_share_group = '<div class="recipe-social-wrapper"><span>' . $share_word . ': </span><div class="social-icon icon-box icon-box-top icon-inline">
	<a href="https://facebook.com/sharer.php?display=page&u=' . get_permalink() . '" target="_blank" alt="Share on Facebook"><i class="fa fa-facebook"></i></a>
	</div>
	<div class="social-icon icon-box icon-box-top icon-inline">
	<a href="http://pinterest.com/pin/create/button/?url=' . get_permalink() . '&media=' . $recipe_thumbnail_file_path . '&description=' . $recipe_name . '" target="_blank"><i class="fa fa-pinterest-square"></i></a></div>
	<div class="social-icon icon-box icon-box-top icon-inline">
	<a href="mailto:?subject=' . $recipe_name . '&body=' . get_permalink() . '" target="_blank"><i class="fa fa-envelope-o"></i></a></div>
	</div>';




	$recipe_tags = '';
	if (have_rows('recipe_tags')) {
		while (have_rows('recipe_tags')) {
			the_row();

			$currTagName = get_sub_field('tag_name');
			$currTagLink = get_sub_field('tag');

			$recipe_tags = $recipe_tags . '<a class="recipe-tag" href="/recipe-listing/?' . $currTagLink . '">' . $currTagName . '</a>';
		}
	}

	$recipe_tags = '<div class="recipe-tags-wrapper">' . $recipe_tags . '</div>';

	// build ingredients, tips and direction

	// get ingredients and tips 
	if (have_rows('ingredients')) {
		$ingredients = '<h4>' . $ingredients_word . '</h4>
						<ul class="ingredient-list">';
		while (have_rows('ingredients')) {
			the_row();

			$currIngredient = get_sub_field('ingredient');
			$currIngredientAmount = get_sub_field('ingredient_amount');
			if (strpos($currIngredient, 'Daisy Sour Cream') !== false || strpos($currIngredient, 'Daisy Light Sour Cream') !== false) {
				$ingredients = $ingredients . '<li><a href="/sour-cream">' . $currIngredientAmount . ' ' . $currIngredient . '</a></li>';
			} elseif (strpos($currIngredient, 'Daisy Cottage Cheese') !== false) {
				if ($my_current_lang == "es") {
					$ingredients = $ingredients . '<li><a href="/es/queso-fresco">' . $currIngredientAmount . ' ' . $currIngredient . '</a></li>';
				} else {
					$ingredients = $ingredients . '<li><a href="/cottage-cheese">' . $currIngredientAmount . ' ' . $currIngredient . '</a></li>';
				}
			} elseif (strpos($currIngredient, 'Crema Daisy') !== false) {
				$ingredients = $ingredients . '<li><a href="/es/nata-agria">' . $currIngredientAmount . ' ' . $currIngredient . '</a></li>';
			} else {
				$ingredients = $ingredients . '<li>' . $currIngredientAmount . ' ' . $currIngredient . '</li>';
			}
		}
		$ingredients = $ingredients . '</ul>';

		if (have_rows('tips')) {
			$tips = '<h4 class="tips-title">' . $tips_word . '</h4>
							<ul class="tips-list">';
			while (have_rows('tips')) {
				the_row();

				$currTipTitle = get_sub_field('tip_title');
				$currTip = get_sub_field('tip');
				$tips = $tips . '<li><h5>' . $currTipTitle . '</h5><p>' . $currTip . '</p></li>';
			}
			$tips = $tips . '</ul>';
		}
		if ($tips) {
			$ingredients = $ingredients . '<hr>' . $tips;
		}
	}

	// get directions
	if (have_rows('recipe_instructions')) {
		$instruction_count = count(get_field('recipe_instructions'));
		if ($instruction_count <= 1) {
			$instructions = '<h4>' . $directions_word . '</h4>';
			while (have_rows('recipe_instructions')) {
				the_row();

				$currInstruction = get_sub_field('instruction');
				$instructions = $instructions . '<p class="single-instruction">' . $currInstruction . '</p>';
			}
		} else {
			$instructions = '<h4>' . $directions_word . '</h4>
			<ol class="instruction-list">';
			while (have_rows('recipe_instructions')) {
				the_row();

				$currInstruction = get_sub_field('instruction');
				$instructions = $instructions . '<li>' . $currInstruction . '</li>';
			}

			$instructions = $instructions . '</ol>';
		}
	}

	// put ingredients and instructions together
	$ingredients_directions = 	'<div class="ingredients-direction-wrapper">
									<div class="ingredients-direction-item left-item">' . $ingredients .
		'</div>
									<div class="ingredients-direction-item">' . $instructions .
		'</div>
								</div>';
	$ingredients_directions_base64 = base64_encode($ingredients_directions);


	// get nutrition facts
	if (have_rows('nutrition_facts')) {
		$nutritionfacts = 	'<ul class="nutrition-list">';
		while (have_rows('nutrition_facts')) {
			the_row();

			$curr_nutritionfacts = get_sub_field('nutrition');
			$curr_nutrition_amount = get_sub_field('nutrition_amount');
			$nutritionfacts = $nutritionfacts . '<li><span class="nutrition-fact-name"><strong>' . $curr_nutritionfacts . ':</strong></span><span>' . $curr_nutrition_amount . '</span></li>';
		}
		$nutritionfacts = $nutritionfacts . '</ul>';
	}
	$nutritionfacts_base64 = base64_encode($nutritionfacts);


	$recipe_bazaar_id = get_field('bazaarvoice_id_pointer');
	$bazaar_rating_summary = '<div data-bv-show="rating_summary" data-bv-product-id="' . $recipe_bazaar_id . '"></div>';
	$bazaar_rating_summary_base64 = base64_encode($bazaar_rating_summary);

	$bazaar_reviews = '<div data-bv-show="reviews" data-bv-product-id="' . $recipe_bazaar_id . '"></div>';
	$bazaar_reviews_base64 = base64_encode($bazaar_reviews);

	$bazaar_catelog_widet = '<script async type="text/javascript">
	window.bvDCC = {
		catalogData: {
			locale: "en_US",
			catalogProducts: [{
				"productId" : "' . $recipe_bazaar_id . '",
				"productName" : "' . $recipe_name . '",
				"productImageURL": "' . wp_get_attachment_image_url($recipe_image_id) . '",
				"productPageURL":"' . get_permalink() . '",
				"brandName" : "Diasy Brand",
				"inactive": false, //default
			}]
		}
	};
	window.bvCallback = function (BV) {
		BV.pixel.trackEvent("CatalogUpdate", {
			type: "Product",
			locale: window.bvDCC.catalogData.locale,
			catalogProducts: window.bvDCC.catalogData.catalogProducts
		});
	};

	sharefb = function(){
		url = "https://work.facebook.com/sharer.php?display=popup&u=" + window.location.href;
		options = "toolbar=0,status=0,resizable=1,width=626,height=436";
		window.open(url,"sharer",options);
	}

	window.bvCallback = function(BV) {
		BV.reviews.on("show", function() {
				jQuery("li.vc_tta-tab").removeClass("active");
				jQuery("li.vc_tta-tab").eq( 1 ).addClass("active");
				jQuery(".tab-pane").removeClass("in active");
				jQuery(".tab-pane").eq(1).addClass("in active");
		}); };
	</script>';

	//[vc_single_image media="331" media_lightbox="yes" media_width_use_pixel="yes" alignment="center"]
	// build slider
	$slide_index = 0;
	$slider = '<div class="my-slider"> 
				<div>' . do_shortcode('[vc_single_image media="' . $recipe_image_id . '" media_lightbox="no" lbox_no_tmb="yes" lbox_no_arrows="yes"  media_ratio="two-three" alignment="center" el_class="recipe-media-wrapper"]') . '</div>';
	$thumb_nav = '<ul class="thumbnails" id="customize-thumbnails" aria-label="Carousel Pagination">
					<li data-nav="0" aria-label="Carousel Page 1 (Current Slide)" aria-controls="customize" class="tns-nav-active">
					' . do_shortcode('[vc_single_image media="' . $recipe_image_id . '" media_lightbox="no" media_ratio="one-one" alignment="center"]') .  '
					</li>';

	if (have_rows('additional_images')) {
		while (have_rows('additional_images')) {
			the_row();
			if (get_sub_field('image')) {
				$slide_index = $slide_index + 1;
				$image = get_sub_field('image');
				$slider = 	$slider . '<div class="addtest">' . do_shortcode('[vc_single_image media="' . $image . '" media_lightbox="no" lbox_no_tmb="yes" lbox_no_arrows="yes"  media_ratio="two-three" alignment="center" el_class="recipe-media-wrapper"]') . '</div>';
				$thumb_nav = $thumb_nav . '<li data-nav="' . $slide_index . '" aria-label="Carousel Page ' . $slide_index . '" aria-controls="customize" class="addtest" tabindex="-1">
					' . do_shortcode('[vc_single_image media="' . $image . '" media_lightbox="no" media_ratio="one-one" alignment="center"]') .  '
					  </li>';
			}
		}
	}

	if (have_rows('videos')) {
		while (have_rows('videos')) {
			the_row();
			if (get_sub_field('video')) {
				$slide_index = $slide_index + 1;
				$video = get_sub_field('video');
				$slider = 	$slider . '<div class="addtest2">' . do_shortcode('[vc_single_image media="' . $video . '" media_lightbox="yes" media_poster="yes" media_width_percent="100" media_ratio="two-three" alignment="center" advanced="yes" media_items="media|poster,icon|md" media_style="dark" media_overlay_opacity="1" media_text_visible="yes" media_text_anim="no" media_overlay_anim="no" media_image_anim="no" media_h_align="center" media_h_position="center" media_padding="2" media_icon="fa fa-play" lbox_no_tmb="yes" lbox_no_arrows="yes" no_double_tap="yes" el_class="recipe-video"]') . '</div>';
				$thumb_nav = $thumb_nav . '<li data-nav="' . $slide_index . '" aria-label="Carousel Page ' . $slide_index . '" aria-controls="customize" class="recipe-video recipe-video__thumbnail" tabindex="-1">
					' . do_shortcode('[vc_single_image media="' . get_post_meta($video, "_uncode_poster_image", true) . '" media_ratio="one-one" alignment="center"]') .  '
					</li>';
			}
		}
	}


	$slider = $slider .	'</div>';
	$thumb_nav = $thumb_nav . '</ul>';

	if ($slide_index >= 1) {
		$slider_content  = $slider . $thumb_nav;
	} else {
		$slider_content = do_shortcode('[vc_single_image media="' . $recipe_image_id . '" media_lightbox="yes" media_ratio="two-three" alignment="center" el_class="recipe-media-wrapper"]');
	}

	$shortcode_test2 = '
	[vc_row row_height_percent="0" overlay_alpha="50" gutter_size="3" column_width_percent="100" shift_y="0" z_index="0"]
		[vc_column width="1/1" font_family="font-421327"]
			[vc_tabs typography="yes"]
				[vc_tab title="' . $ingredient_direction_word . '" tab_id="1574119948-1-14"]
					[vc_raw_html]' . $ingredients_directions_base64 . '[/vc_raw_html]
				[/vc_tab]'
		. (ICL_LANGUAGE_CODE == 'en' ? '[vc_tab title="Reviews" tab_id="1574119948-2-76"]
					[vc_raw_html]' . $bazaar_reviews_base64 . '[/vc_raw_html]
				[/vc_tab]' : '') . '
				[vc_tab title="' . $nutrition_info_word . '" tab_id="1574120172930-2-0"]
					[vc_raw_html]' . $nutritionfacts_base64 . '[/vc_raw_html]
				[/vc_tab]
			[/vc_tabs]
			[vc_separator]
		[/vc_column]
	[/vc_row]';

	echo '<div class="recipe-detail-wrapper" data-lang="' . ICL_LANGUAGE_CODE . '">
			<div class="first-recipe-row">
				<div class="first-recipe-row-left">'
		. $slider_content .
		'</div>
				<div class="first-recipe-row-right">'
		. $recipe_heading . (ICL_LANGUAGE_CODE == 'en' ? $bazaar_rating_summary : '') . $recipe_times . $cta_btn_group . $recipe_share_group . do_shortcode('[vc_separator]') . $recipe_tags .
		'<div class="ingredients-print">' . $ingredients . '</div>	
				</div>
			</div>';


	echo do_shortcode($shortcode_test2) . '</div>';

	// Start recommendations
	$ignore_manual_recommendation = get_field('manual_recommendation');
	$first_recommendation = str_replace('https://daisybrand.azurewebsites.net', '', get_field('first_recommendation'));
	$second_recommendation = str_replace('https://daisybrand.azurewebsites.net', '', get_field('second_recommendation'));
	$third_recommendation = str_replace('https://daisybrand.azurewebsites.net', '', get_field('third_recommendation'));
	// try to get post id from url
	if ($first_recommendation && $second_recommendation && $third_recommendation) {
		$first_recommendation_id = url_to_postid($first_recommendation);
		$second_recommendation_id = url_to_postid($second_recommendation);
		$third_recommendation_id = url_to_postid($third_recommendation);
	}
	// try to get featured image from id
	if ($first_recommendation_id && $second_recommendation_id && $third_recommendation_id) {
		$first_recommendation_featured_image = get_the_post_thumbnail($first_recommendation_id);
		$second_recommendation_featured_image = get_the_post_thumbnail($second_recommendation_id);
		$third_recommendation_featured_image = get_the_post_thumbnail($third_recommendation_id);
	}

	// check that all 3 recipe posts a valid before choosing to display instead of recommendtion pluggin
	if ($first_recommendation_featured_image && $second_recommendation_featured_image && $third_recommendation_featured_image && !$ignore_manual_recommendation) {
		$recommendation_html = '<div class="rp4wp-related-posts rp4wp-related-recipe DAVID">
			<ul class="rp4wp-posts-list">
				<li class="rp4wp-col rp4wp-col-first">
					<div class="rp4wp_component rp4wp_component_image rp4wp_component_2">
						<a href="' . $first_recommendation . '">
						' . $first_recommendation_featured_image . '
						</a>
					</div>
					<div class="rp4wp_component rp4wp_component_title rp4wp_component_3">
						<a href="' . $first_recommendation . '">' . get_the_title($first_recommendation_id) . '</a>
					</div>
				</li>
				<li class="rp4wp-col">
					<div class="rp4wp_component rp4wp_component_image rp4wp_component_2">
						<a href="' . $second_recommendation . '">
							' . $second_recommendation_featured_image . '
						</a>
					</div>
					<div class="rp4wp_component rp4wp_component_title rp4wp_component_3">
						<a href="' . $second_recommendation . '">' . get_the_title($second_recommendation_id) . '</a>
					</div>
				</li>
				<li class="rp4wp-col rp4wp-col-last">
					<div class="rp4wp_component rp4wp_component_image rp4wp_component_2">
						<a href="' . $third_recommendation . '">
							' . $third_recommendation_featured_image . '
						</a>
					</div>
					<div class="rp4wp_component rp4wp_component_title rp4wp_component_3">
						<a href="' . $third_recommendation . '">' . get_the_title($third_recommendation_id) . '</a>
					</div>
				</li>
			</ul>
		</div>';
		$recomendations_base64 = base64_encode($recommendation_html);
		echo do_shortcode('[vc_row el_class="crp-wrapper"][vc_column column_width_percent="100" align_horizontal="align_center" gutter_size="3" overlay_alpha="50" shift_x="0" shift_y="0" shift_y_down="0" z_index="0" medium_width="0" mobile_width="0"][vc_custom_heading text_color="sp-daisy-blue"]' . $recomendations_word . '[/vc_custom_heading][vc_separator sep_color="sp-color-red" el_width="140px" el_height="5px"][/vc_column][/vc_row][vc_raw_html]' . $recomendations_base64 . '[/vc_raw_html]');
	} else {
		echo do_shortcode('[vc_row el_class="crp-wrapper"][vc_column column_width_percent="100" align_horizontal="align_center" gutter_size="3" overlay_alpha="50" shift_x="0" shift_y="0" shift_y_down="0" z_index="0" medium_width="0" mobile_width="0"][vc_custom_heading text_color="sp-daisy-blue"]' . $recomendations_word . '[/vc_custom_heading][vc_separator sep_color="sp-color-red" el_width="140px" el_height="5px"][/vc_column][/vc_row][rp4wp]');
	}
	// End recommenations
	echo $bazaar_catelog_widet;






















	/** Build content **/

	$the_content = get_the_content();
	if (has_shortcode($the_content, 'vc_row')) {
		$with_builder = true;
	}

	if (!$with_builder) {
		$the_content = apply_filters('the_content', $the_content);
		$the_content = $title_content . $the_content;
		if ($media_content !== '') {
			$the_content = $media_content . $the_content;
		}
	} else {
		$get_content_appended = apply_filters('the_content', '');
		if (!is_null($get_content_appended) && $get_content_appended !== '') {
			$the_content = $the_content . uncode_get_row_template($get_content_appended, $limit_width, $limit_content_width, $style, '', false, true, 'double', $page_custom_width);
		}
	}

	$the_content .= wp_link_pages(array(
		'before' => '<div class="page-links">' . esc_html__('Pages:', 'uncode'),
		'after'  => '</div>',
		'link_before'	=> '<span>',
		'link_after'	=> '</span>',
		'echo'	=> 0
	));

	/** Build tags **/

	$page_show_tags = (isset($metabox_data['_uncode_specific_tags'][0])) ? $metabox_data['_uncode_specific_tags'][0] : '';
	if ($page_show_tags === '') {
		$generic_show_tags = ot_get_option('_uncode_' . $post_type . '_tags');
		$show_tags = ($generic_show_tags === 'off') ? false : true;
		if ($show_tags) {
			$show_tags_align = ot_get_option('_uncode_' . $post_type . '_tags_align');
		}
	} else {
		$show_tags = ($page_show_tags === 'off') ? false : true;
		if ($show_tags) {
			$show_tags_align = (isset($metabox_data['_uncode_specific_tags_align'][0])) ? $metabox_data['_uncode_specific_tags_align'][0] : '';
		}
	}

	if ($show_tags) {
		$tag_ids = wp_get_post_tags($post->ID, array('fields' => 'ids'));
		if ($tag_ids) {
			$args = array(
				'smallest'                  => 11,
				'largest'                   => 11,
				'unit'                      => 'px',
				'include'                   => $tag_ids,
				'taxonomy'                  => 'post_tag',
				'echo'                      => false,
				'number'										=> 999,
			);

			$tag_cloud = '<div class="widget-container post-tag-container uncont text-' . $show_tags_align . '"><div class="tagcloud">' . wp_tag_cloud($args) . '</div></div>';

			if (!$with_builder) {
				$the_content .= $tag_cloud;
			} else {
				$the_content .= uncode_get_row_template($tag_cloud, $limit_width, $limit_content_width, $style, '', false, true, 'double', $page_custom_width);
			}
		}
	}

	/** JetPack related posts **/

	if (shortcode_exists('jetpack-related-posts')) {
		$related_content = do_shortcode('[jetpack-related-posts]');
		if ($related_content !== '') {
			if (!$with_builder) {
				$the_content .= $related_content;
			} else {
				$the_content .= uncode_get_row_template($related_content, $limit_width, $limit_content_width, $style, '', false, true, 'double', $page_custom_width);
			}
		}
	}

	/** Build post after block **/
	$content_after_body = '';
	$page_content_blocks_after = array(
		'above' => '_pre',
		'below' => ''
	);

	foreach ($page_content_blocks_after as $order => $pre) {

		$content_after_body_build = '';

		$page_content_block_after = (isset($metabox_data['_uncode_specific_content_block_after' . $pre][0])) ? $metabox_data['_uncode_specific_content_block_after' . $pre][0] : '';
		if ($page_content_block_after === '') {
			$generic_content_block_after = ot_get_option('_uncode_' . $post_type . '_content_block_after' . $pre);
			$content_block_after = $generic_content_block_after !== '' ? $generic_content_block_after : false;
		} else {
			$content_block_after = $page_content_block_after !== 'none' ? $page_content_block_after : false;
		}

		if ($content_block_after !== false) {
			$content_block_after = apply_filters('wpml_object_id', $content_block_after, 'post');
			$content_after_body_build = get_post_field('post_content', $content_block_after);
			if (class_exists('Vc_Base')) {
				$vc = new Vc_Base();
				$vc->addShortcodesCustomCss($content_block_after);
			}
			if (has_shortcode($content_after_body_build, 'vc_row')) {
				$content_after_body_build = '<div class="post-after row-container">' . $content_after_body_build . '</div>';
			} else {
				$content_after_body_build = '<div class="post-after row-container">' . uncode_get_row_template($content_after_body_build, $limit_width, $limit_content_width, $style, '', false, true, 'double', $page_custom_width) . '</div>';
			}
			if (class_exists('RP4WP_Post_Link_Manager')) {
				if (is_array(RP4WP::get()->settings)) {
					$automatic_linking_post_amount = RP4WP::get()->settings['general_' . $post_type]->get_option('automatic_linking_post_amount');
				} else {
					$automatic_linking_post_amount = RP4WP::get()->settings->get_option('automatic_linking_post_amount');
				}
				$uncode_related = new RP4WP_Post_Link_Manager();
				$related_posts = $uncode_related->get_children($post->ID, false);
				$related_posts_ids = array();
				foreach ($related_posts as $key => $value) {
					if (isset($value->ID)) {
						$related_posts_ids[] = $value->ID;
					}
				}
				$archive_query = '';
				$regex = '/\[uncode_index(.*?)\]/';
				$regex_attr = '/(.*?)=\"(.*?)\"/';
				preg_match_all($regex, $content_after_body_build, $matches, PREG_SET_ORDER);
				foreach ($matches as $key => $value) {
					$index_found = false;
					if (isset($value[1])) {
						preg_match_all($regex_attr, trim($value[1]), $matches_attr, PREG_SET_ORDER);
						foreach ($matches_attr as $key_attr => $value_attr) {
							switch (trim($value_attr[1])) {
								case 'auto_query':
									if ($value_attr[2] === 'yes') {
										$index_found = true;
									}
									break;
								case 'loop':
									$archive_query = $value_attr[2];
									break;
							}
						}
					}
					if ($index_found) {
						if ($archive_query === '') {
							$archive_query = ' loop="size:10|by_id:' . implode(',', $related_posts_ids) . '|post_type:' . $post->post_type . '"';
						} else {
							$parse_query = uncode_parse_loop_data($archive_query);
							$parse_query['by_id'] = implode(',', $related_posts_ids);
							if (!isset($parse_query['order'])) {
								$parse_query['order'] = 'none';
							}
							$archive_query = ' loop="' . uncode_unparse_loop_data($parse_query) . '"';
						}
						$value[1] = preg_replace('#\s(loop)="([^"]+)"#', $archive_query, $value[1], -1, $index_count);
						if ($index_count === 0) {
							$value[1] .= $archive_query;
						}
						$replacement = '[uncode_index' . $value[1] . ']';
						$content_after_body_build = str_replace($value[0], $replacement, $content_after_body_build);
					}
				}
			}
		}

		$content_after_body .= $content_after_body_build;
	}

	/** Build post footer **/

	$page_show_share = (isset($metabox_data['_uncode_specific_share'][0])) ? $metabox_data['_uncode_specific_share'][0] : '';
	if ($page_show_share === '') {
		$generic_show_share = ot_get_option('_uncode_' . $post_type . '_share');
		$show_share = ($generic_show_share === 'off') ? false : true;
	} else {
		$show_share = ($page_show_share === 'off') ? false : true;
	}

	if ($show_share) {
		$footer_content = '<div class="post-share">
	          						<div class="detail-container margin-auto">
													<div class="share-button share-buttons share-inline only-icon"></div>
												</div>
											</div>';
	}

	$show_comments = ot_get_option('_uncode_' . $post_type . '_comments');

	if ((comments_open() || '0' != get_comments_number()) && $show_comments === 'on') {
		$footer_content .= '<div data-name="commenta-area">';
		ob_start();
		comments_template();
		$footer_content .= ob_get_clean();
		$footer_content .= '</div>';
	}

	if ($layout === 'sidebar_right' || $layout === 'sidebar_left') {

		/** Build structure with sidebar **/

		if ($sidebar_size === '') {
			$sidebar_size = 4;
		}
		$main_size = 12 - $sidebar_size;
		$expand_col = '';

		/** Collect paddings data **/

		$footer_classes = ' no-top-padding double-bottom-padding';

		if ($sidebar_bg_color !== '') {
			if ($sidebar_fill === 'on') {
				$sidebar_inner_padding .= ' std-block-padding';
				$sidebar_padding .= $sidebar_bg_color;
				$expand_col = ' unexpand';
				if ($limit_content_width === '') {
					$row_classes .= ' no-h-padding col-no-gutter no-top-padding';
					$footer_classes = ' std-block-padding no-top-padding';
					if (!$with_builder) {
						$main_classes .= ' std-block-padding';
					}
				} else {
					$row_classes .= ' no-top-padding';
					if (!$with_builder) {
						$main_classes .= ' double-top-padding';
					}
				}
			} else {
				$row_classes .= ' double-top-padding';
				$row_classes .= ' double-bottom-padding';
				$sidebar_inner_padding .= $sidebar_bg_color . ' single-block-padding';
			}
		} else {
			if ($with_builder) {
				if ($limit_content_width === '') {
					$row_classes .= ' col-std-gutter no-top-padding';
					if ($boxed !== 'on') {
						$row_classes .= ' no-h-padding';
					}
				} else {
					$row_classes .= ' col-std-gutter no-top-padding';
				}
				$sidebar_inner_padding .= ' double-top-padding';
			} else {
				$row_classes .= ' col-std-gutter double-top-padding';
				$main_classes .= ' double-bottom-padding';
			}
		}

		$row_classes .= ' no-bottom-padding';
		$sidebar_inner_padding .= ' double-bottom-padding';

		/** Build sidebar **/

		$sidebar_content = "";
		ob_start();
		if ($sidebar !== '') {
			dynamic_sidebar($sidebar);
		} else {
			dynamic_sidebar(1);
		}
		$sidebar_content = ob_get_clean();

		/** Create html with sidebar **/

		if ($footer_content !== '') {
			if ($limit_content_width === '') {
				$footer_content = uncode_get_row_template($footer_content, $limit_width, $limit_content_width, $style, '', false, true, '');
			}
			$footer_content = '<div class="post-footer post-footer-' . $style . ' style-' . $style . $footer_classes . '">' . $footer_content . '</div>';
		}

		$the_content = '<div class="post-content style-' . $style . $main_classes . '">' . $the_content . '</div>';

		$main_content = '<div class="col-lg-' . $main_size . '">
											' . $the_content . $content_after_body . $footer_content . '
										</div>';

		$the_content = '<div class="row-container">
        							<div class="row row-parent un-sidebar-layout' . $row_classes . $limit_content_width . '"' . $page_custom_width . '>
												<div class="row-inner">
													' . (($layout === 'sidebar_right') ? $main_content : '') . '
													<div class="col-lg-' . $sidebar_size . '">
														<div class="uncol style-' . $sidebar_style . $expand_col . $sidebar_padding . (($sidebar_fill === 'on' && $sidebar_bg_color !== '') ? '' : $sidebar_sticky) . '">
															<div class="uncoltable' . (($sidebar_fill === 'on' && $sidebar_bg_color !== '') ? $sidebar_sticky : '') . '">
																<div class="uncell' . $sidebar_inner_padding . '">
																	<div class="uncont">
																		' . $sidebar_content . '
																	</div>
																</div>
															</div>
														</div>
													</div>
													' . (($layout === 'sidebar_left') ? $main_content : '') . '
												</div>
											</div>
										</div>';
	} else {

		/** Create html without sidebar **/
		if ($with_builder) {
			$the_content = '<div class="post-content un-no-sidebar-layout">' . $the_content . '</div>';
		} else {
			$uncode_get_row_template = uncode_get_row_template($the_content, $limit_width, $limit_content_width, $style, '', 'double', true, 'double');
			if ($uncode_get_row_template != '') {
				$the_content = '<div class="post-content un-no-sidebar-layout"' . $page_custom_width . '>' . $uncode_get_row_template . '</div>';
			}
		}
		$the_content .= $content_after_body;
		$uncode_get_row_template = uncode_get_row_template($footer_content, $limit_width, $limit_content_width, $style, '', false, true, 'double', $page_custom_width);
		if ($uncode_get_row_template != '') {
			$the_content .= '<div class="post-footer post-footer-' . $style . ' row-container">' . $uncode_get_row_template . '</div>';
		}
	}

	/** Build and display navigation html **/
	$navigation_option = ot_get_option('_uncode_' . $post_type . '_navigation_activate');
	if ($navigation_option !== 'off') {
		$generic_index = true;
		if (isset($metabox_data['_uncode_specific_navigation_index'][0]) && $metabox_data['_uncode_specific_navigation_index'][0] !== '') {
			$navigation_index = $metabox_data['_uncode_specific_navigation_index'][0];
			$generic_index = false;
		} else {
			$navigation_index = ot_get_option('_uncode_' . $post_type . '_navigation_index');
		}
		if ($navigation_index !== '') {
			$navigation_index_label = ot_get_option('_uncode_' . $post_type . '_navigation_index_label');
			$navigation_index_link = get_permalink($navigation_index);
			$navigation_index_btn = '<a class="btn btn-link text-default-color" href="' . esc_url($navigation_index_link) . '">' . ($navigation_index_label === '' ? get_the_title($navigation_index) : esc_html($navigation_index_label)) . '</a>';
		} else {
			$navigation_index_btn = '';
		}
		$navigation_nextprev_title = ot_get_option('_uncode_' . $post_type . '_navigation_nextprev_title');
		$navigation = uncode_post_navigation($navigation_index_btn, $navigation_nextprev_title, $navigation_index, $generic_index);
		if ($page_custom_width !== '') {
			$limit_content_width = ' limit-width';
		}
		if (!empty($navigation) && $navigation !== '') {
			$navigation_content = uncode_get_row_template($navigation, '', $limit_content_width, $style, ' row-navigation row-navigation-' . $style, true, true, true);
		}
	}

	/** Display post html **/
// echo 	'<article id="post-'. get_the_ID().'" class="'.implode(' ', get_post_class('page-body' . $bg_color)) .'">
//       <div class="post-wrapper">
//       	<div class="post-body">' . uncode_remove_p_tag($the_content) . '</div>' .
//       	$navigation_content . '
//       </div>
//     </article>';

endwhile;
// end of the loop.

get_footer(); ?>