<?php
add_action('wp_enqueue_scripts', 'child_enqueue_styles', 99);

function child_enqueue_styles()
{
	$parent_style = 'parent-style';
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));
	wp_enqueue_style('theme', get_stylesheet_directory_uri() . '/css/style.css');
}

if (get_stylesheet() !== get_template()) {
	add_filter('pre_update_option_theme_mods_' . get_stylesheet(), function ($value, $old_value) {
		update_option('theme_mods_' . get_template(), $value);
		return $old_value; // prevent update to child theme mods
	}, 10, 2);
	add_filter('pre_option_theme_mods_' . get_stylesheet(), function ($default) {
		return get_option('theme_mods_' . get_template(), $default);
	});
}

function wpb_adding_scripts()
{

	wp_register_script('main_script', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery'), '1.0', true);

	wp_enqueue_script('main_script');
}

add_action('wp_enqueue_scripts', 'wpb_adding_scripts');

// contact form date picker fallback
add_filter('wpcf7_support_html5_fallback', '__return_true');

// Recipes Custom Post Type 
add_action('init', 'recipe_cpt');

function recipe_cpt()
{

	register_post_type('recipe', array(
		'labels' => array(
			'name' => 'Recipes',
			'singular_name' => 'Recipe'
		),
		'description' => 'Recipes that will be displayed across the site.',
		'public' => true,
		'menu_position' => 20,
		'supports' => array('title', 'custom-fields',  'thumbnail', 'page-attributes'),
		'rewrite' => array('slug' => 'recipes')
	));
}

// Holiday Recipes Custom Post Type 
add_action('init', 'holiday_recipe_cpt');

function holiday_recipe_cpt()
{

	register_post_type('holiday_recipe', array(
		'labels' => array(
			'name' => 'Holiday Recipes',
			'singular_name' => 'Holiday Recipe'
		),
		'description' => 'Holiday recipes that will NOT be displayed across the site.',
		'public' => true,
		'has_archive' => false,
		'menu_position' => 21,
		'exclude_from_search' => true,
		'supports' => array('title', 'custom-fields',  'thumbnail', 'page-attributes'),
		'rewrite' => array(
			'slug' => 'holiday_recipe',
			'with_front' => false
		)
	));
}

/**
 * Remove the slug from published post permalinks. Only affect our custom post type, though.
 */
function gp_remove_cpt_slug($post_link, $post)
{
	if ('holiday_recipe' === $post->post_type && 'publish' === $post->post_status) {
		$post_link = home_url($post->post_name);
	}
	return $post_link;
}
add_filter('post_type_link', 'gp_remove_cpt_slug', 10, 2);
function custom_post_type_rewrites()
{
	add_rewrite_rule('([^/]+)(?:/([0-9]+))?/?$', 'index.php?holiday_recipe=$matches[1]', 'bottom');
}
add_action('init', 'custom_post_type_rewrites');

// Custom FacetWP sort by
add_filter('facetwp_sort_options', function ($options, $params) {
	$options['default'] = array(
		'label' => 'Featured',
		'query_args' => array(
			'orderby' => 'menu_order date',
			'order' => 'DESC',
		)
	);
	return $options;
}, 10, 2);

// hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'create_recipe_taxonomies', 0);

// set bazaarvoice id if none on recipe creation
function my_acf_prepare_field($field)
{
	$field['readonly'] = true; 	// if already has value, return it
	if ($field['value']) {
		return $field;
	}

	$d1 = new Datetime();
	$field['value'] = $d1->format('U');

	return $field;
}

add_filter('acf/prepare_field/name=bazaarvoice_id', 'my_acf_prepare_field');

// create five taxonomies for "recipe"
function create_recipe_taxonomies()
{

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x('Recipe Category', 'taxonomy general name', 'textdomain'),
		'singular_name'              => _x('Recipe Category', 'taxonomy singular name', 'textdomain'),
		'search_items'               => __('Search Categories', 'textdomain'),
		'popular_items'              => __('Popular Categories', 'textdomain'),
		'all_items'                  => __('All Categories', 'textdomain'),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __('Edit Recipe Category', 'textdomain'),
		'update_item'                => __('Update Recipe Category', 'textdomain'),
		'add_new_item'               => __('Add New Recipe Category', 'textdomain'),
		'new_item_name'              => __('New Recipe Category Name', 'textdomain'),
		'separate_items_with_commas' => __('Separate categories with commas', 'textdomain'),
		'add_or_remove_items'        => __('Add or remove categories', 'textdomain'),
		'choose_from_most_used'      => __('Choose from the most used categories', 'textdomain'),
		'not_found'                  => __('No recipe categories found.', 'textdomain'),
		'menu_name'                  => __('Recipe Categories', 'textdomain'),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array('slug' => 'recipe-category'),
	);

	register_taxonomy('recipe-category', 'recipe', $args);

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x('Season', 'taxonomy general name', 'textdomain'),
		'singular_name'              => _x('Season', 'taxonomy singular name', 'textdomain'),
		'search_items'               => __('Search Seasons', 'textdomain'),
		'popular_items'              => __('Popular Seasons', 'textdomain'),
		'all_items'                  => __('All Seasons', 'textdomain'),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __('Edit Season', 'textdomain'),
		'update_item'                => __('Update Season', 'textdomain'),
		'add_new_item'               => __('Add New Season', 'textdomain'),
		'new_item_name'              => __('New Season Name', 'textdomain'),
		'separate_items_with_commas' => __('Separate seasons with commas', 'textdomain'),
		'add_or_remove_items'        => __('Add or remove seasons', 'textdomain'),
		'choose_from_most_used'      => __('Choose from the most used seasons', 'textdomain'),
		'not_found'                  => __('No Seasons found.', 'textdomain'),
		'menu_name'                  => __('seasons', 'textdomain'),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array('slug' => 'season'),
	);

	register_taxonomy('season', 'recipe', $args);

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x('Dietary', 'taxonomy general name', 'textdomain'),
		'singular_name'              => _x('Dietary', 'taxonomy singular name', 'textdomain'),
		'search_items'               => __('Search Dietaries', 'textdomain'),
		'popular_items'              => __('Popular Dietaries', 'textdomain'),
		'all_items'                  => __('All Dietaries', 'textdomain'),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __('Edit Dietary', 'textdomain'),
		'update_item'                => __('Update Dietary', 'textdomain'),
		'add_new_item'               => __('Add New Dietary', 'textdomain'),
		'new_item_name'              => __('New Dietary Name', 'textdomain'),
		'separate_items_with_commas' => __('Separate dietaries with commas', 'textdomain'),
		'add_or_remove_items'        => __('Add or remove dietaries', 'textdomain'),
		'choose_from_most_used'      => __('Choose from the most used dietaries', 'textdomain'),
		'not_found'                  => __('No Dietaries found.', 'textdomain'),
		'menu_name'                  => __('dietaries', 'textdomain'),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array('slug' => 'dietary'),
	);

	register_taxonomy('dietary', 'recipe', $args);


	register_taxonomy('dietary', 'recipe', $args);

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x('Meal Type', 'taxonomy general name', 'textdomain'),
		'singular_name'              => _x('Meal Type', 'taxonomy singular name', 'textdomain'),
		'search_items'               => __('Search Meal Types', 'textdomain'),
		'popular_items'              => __('Popular Meal Types', 'textdomain'),
		'all_items'                  => __('All Meal Types', 'textdomain'),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __('Edit Meal Type', 'textdomain'),
		'update_item'                => __('Update Meal Type', 'textdomain'),
		'add_new_item'               => __('Add New Meal Type', 'textdomain'),
		'new_item_name'              => __('New Meal Type Name', 'textdomain'),
		'separate_items_with_commas' => __('Separate Meal Types with commas', 'textdomain'),
		'add_or_remove_items'        => __('Add or remove Meal Types', 'textdomain'),
		'choose_from_most_used'      => __('Choose from the most used Meal Types', 'textdomain'),
		'not_found'                  => __('No Meal Types found.', 'textdomain'),
		'menu_name'                  => __('meal types', 'textdomain'),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array('slug' => 'meal-type'),
	);

	register_taxonomy('meal-type', 'recipe', $args);

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x('Product', 'taxonomy general name', 'textdomain'),
		'singular_name'              => _x('Product', 'taxonomy singular name', 'textdomain'),
		'search_items'               => __('Search Products', 'textdomain'),
		'popular_items'              => __('Popular Products', 'textdomain'),
		'all_items'                  => __('All Products', 'textdomain'),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __('Edit Products', 'textdomain'),
		'update_item'                => __('Update Products', 'textdomain'),
		'add_new_item'               => __('Add New Products', 'textdomain'),
		'new_item_name'              => __('New Products Name', 'textdomain'),
		'separate_items_with_commas' => __('Separate Products with commas', 'textdomain'),
		'add_or_remove_items'        => __('Add or remove Products', 'textdomain'),
		'choose_from_most_used'      => __('Choose from the most used Products', 'textdomain'),
		'not_found'                  => __('No Products found.', 'textdomain'),
		'menu_name'                  => __('product', 'textdomain'),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array('slug' => 'product'),
	);

	register_taxonomy('product', 'recipe', $args);

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x('Occasion', 'taxonomy general name', 'textdomain'),
		'singular_name'              => _x('Occasion', 'taxonomy singular name', 'textdomain'),
		'search_items'               => __('Search Occasions', 'textdomain'),
		'popular_items'              => __('Popular Occasions', 'textdomain'),
		'all_items'                  => __('All Occasions', 'textdomain'),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __('Edit Occasions', 'textdomain'),
		'update_item'                => __('Update Occasions', 'textdomain'),
		'add_new_item'               => __('Add New Occasions', 'textdomain'),
		'new_item_name'              => __('New Occasions Name', 'textdomain'),
		'separate_items_with_commas' => __('Separate Occasions with commas', 'textdomain'),
		'add_or_remove_items'        => __('Add or remove Occasions', 'textdomain'),
		'choose_from_most_used'      => __('Choose from the most used Occasions', 'textdomain'),
		'not_found'                  => __('No Occasions found.', 'textdomain'),
		'menu_name'                  => __('occasion', 'textdomain'),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array('slug' => 'occasion'),
	);

	register_taxonomy('occasion', 'recipe', $args);
}

// reduce relevanssi search to prevent errors
add_filter('pre_option_relevanssi_throttle_limit', function ($limit) {
	return 100;
});

//email validation
add_filter('wpcf7_validate_email*', 'custom_email_confirmation_validation_filter', 20, 2);
function custom_email_confirmation_validation_filter($result, $tag)
{
	if ('your-email' == $tag->name) {
		$your_email = isset($_POST['your-email']) ? trim($_POST['your-email']) : '';
		$domain = explode("@", $your_email)[1];
		if (!checkdnsrr($domain, 'MX')) {
			$result->invalidate($tag, "The e-mail address entered is invalid.");
		}
	}
	return $result;
}


// Prevent Multi Submit on WPCF7 forms
add_action('wp_footer', 'mycustom_wp_footer');

function mycustom_wp_footer()
{
?>

	<script type="text/javascript">
		var disableSubmit = false;
		jQuery('input.wpcf7-submit[type="submit"]').click(function() {
			jQuery(':input[type="submit"]').attr('value', "Sending...")
			if (disableSubmit == true) {
				return false;
			}
			disableSubmit = true;
			return true;
		})

		var wpcf7Elm = document.querySelector('.wpcf7');
		wpcf7Elm.addEventListener('wpcf7submit', function(event) {
			jQuery(':input[type="submit"]').attr('value', "send")
			disableSubmit = false;
		}, false);
	</script>
<?php
}

// Search page redirect 
add_action('template_redirect', 'wp_change_search_url');
function wp_change_search_url()
{
	if (is_search() && !empty($_GET['s'])) {
		wp_redirect(home_url("/search/") . urlencode(get_query_var('s')) . "?term=" . urlencode(get_query_var('s')));
		exit();
	}
}

// Disable Lazy Load on recipe pages
add_filter( 'lazyload_is_enabled', 'lazyload_exclude' );
function lazyload_exclude() {
    if ( get_post_type() == 'recipe') {
        return false;
    } else {
        return true;
    }
}

include('custom-shortcodes.php');
get_template_part( 'partials/menus' );

vc_add_param('vc_single_image', 	
	array(
		'type'          => 'textfield',
		'holder'        => 'div',
		'heading'       => __( 'Aria Label ', 'sagepath' ),
		'param_name'    => 'aria_label',
		'value'         => __( '', 'sagepath' ),
		'description'   => __( 'Aria Label', 'sagepath' ),
	),
);

//remove hreflang if the page is the recipe listing page
add_filter('icl_ls_languages', 'wpml_ls_filter');
function wpml_ls_filter($languages) {
	$current_url = home_url($_SERVER['REQUEST_URI']);

    if(strpos($current_url, 'recipe-listing') || strpos($current_url, 'lista-de-recetas')){
        unset($languages['es']);
		unset($languages['en']);
    }
	return $languages;
} 
