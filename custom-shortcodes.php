<?php

function recipe_carousel_main_func( $atts = array() ) {
  
    // set up default parameters
    extract(shortcode_atts(array(
     'recipe_thumbnail_url' => '#',
     'slide_index' => '',
     'alt_text' => ''
    ), $atts));
    
    return '<div class="uncode-single-media  recipe-media-wrapper text-center">
                <div class="single-wrapper" style="max-width: 100%;">
                    <div class="tmb tmb-light  tmb-img-ratio tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg">
                        <div class="t-inside">
                            <div class="t-entry-visual">
                                <div class="t-entry-visual-tc">
                                    <div class="uncode-single-media-wrapper">
                                        <div class="dummy" style="padding-top: 150.2%;"></div>
                                        <img class="recipe-carousel-main-image" src="' . $recipe_thumbnail_url . '" alt="' . $alt_text . ' slider image ' . $slide_index . '">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
}

function recipe_carousel_thumbnail_func( $atts = array() ) {
  
    // set up default parameters
    extract(shortcode_atts(array(
     'recipe_thumbnail_url' => '#',
     'slide_index' => '',
     'alt_text' => ''
    ), $atts));
    
    return '<div class="uncode-single-media  text-center">
                <div class="single-wrapper" style="max-width: 100%;">
                    <div class="tmb tmb-light  tmb-img-ratio tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg">
                        <div class="t-inside">
                            <div class="t-entry-visual">
                                <div class="t-entry-visual-tc">
                                    <div class="uncode-single-media-wrapper">
                                        <div class="dummy" style="padding-top: 100%;"></div>
                                        <img class="recipe-carousel-thumbnail-image" src="' . $recipe_thumbnail_url . '"' . ' alt="' . $alt_text . ' thumbnail image ' . $slide_index . '">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
}


add_shortcode('recipe_carousel_main', 'recipe_carousel_main_func');
add_shortcode('recipe_carousel_thumbnail', 'recipe_carousel_thumbnail_func');

?>
