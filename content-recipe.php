<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package uncode
 */

$post_type = get_post_type();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<a href="<?php esc_url(the_permalink()); ?>">
		<div class="entry-summary">
			<?php
				$text = wp_strip_all_tags(do_shortcode(get_the_content()));
				echo uncode_truncate($text, 400);
			?>
			
			<?php if ( has_post_thumbnail() ) : ?>
			<div class="row">
				<div class="column">
				<?php the_post_thumbnail( '', array( 'class' => 'entry-thumb', 'alt' => 'Thumbnail image for ' . get_the_title() ) ); ?>
				</div>
			</div>
			<?php endif; ?>
			
		</div><!-- .entry-summary -->
		<div class="entry-header">
			<h4 class="entry-title h4"><?php the_title(); ?></h4>
		</div><!-- .entry-header -->
	</a>
</article><!-- #post-## -->
