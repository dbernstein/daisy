<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="page-wrapper">
 *
 * @package uncode
 */

global $is_redirect, $redirect_page, $wp, $template;

if ($redirect_page) {
	$post_id = $redirect_page;
} else {
	if (isset(get_queried_object()->ID) && !is_home()) {
		$post_id = get_queried_object()->ID;
	} else {
		$post_id = null;
	}
}

if (wp_is_mobile()) {
	$html_class = 'touch';
} else {
	$html_class = 'no-touch';
}

if (is_admin_bar_showing()) {
	$html_class .= ' admin-mode';
}

$current_lang = apply_filters('wpml_current_language', NULL);

?><!DOCTYPE html>
<html class="<?php echo esc_attr($html_class); ?>" lang="<?php echo $current_lang; ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>">
<?php if (wp_is_mobile()) : ?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<?php else : ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php endif; ?>
<link rel="profile" href="http://gmpg.org/xfn/11">
<!-- fonts -->
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6993094/6016212/css/fonts.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-M6K8QVS');</script><!-- End Google Tag Manager -->

<!-- Bazaar Voice --><script async type="text/javascript" src="https://apps.bazaarvoice.com/deployments/daisybrand/main_site/production/en_US/bv.js"></script> <!-- End Bazaar Voice -->
<!-- Rich Pins -->
<meta property="og:site_name" content="daisybrand.com" />
<?php
if ( basename( $template ) === 'single-recipe.php' ) 
{
	$api_passkey = '2ep9qqppzlcqsdvh6r0wd833w';
	$product_id = get_field('bazaarvoice_id');
	$filter = '&Filter=ProductId:' . $product_id . '&Include=Products&Stats=Reviews';
	$url = 'https://api.bazaarvoice.com/data/reviews.json?apiversion=5.4&passkey=' . $api_passkey . $filter;

	try 
	{
		$obj = json_decode(@file_get_contents($url), true);
		$rating_value = $obj['Includes']['Products'][$product_id]['ReviewStatistics']['AverageOverallRating'];
		$rating_count = $obj['Includes']['Products'][$product_id]['ReviewStatistics']['TotalReviewCount'];
		$reviews = $obj['Results'];

	}
	catch(Exception $e)
	{
		$rating_value = '';
		$rating_count = '';
	}


	$nutrition_facts = new stdClass();;
	$recipe_name = get_field('recipe_name');
	$recipe_prep_hours = get_field('prep_time_hours');
	$recipe_prep_minutes = get_field('prep_time_minutes');
	$recipe_time_to_table_hours = get_field('time_to_table_hours');
	$recipe_time_to_table_minutes = get_field('time_to_table_minutes');
	$recipe_yield = get_field('serves');
	$recipe_url = home_url($wp->request);
	$recipe_image_id = get_post_thumbnail_id();
	$recipe_thumbnail_url = wp_get_attachment_url( $recipe_image_id );
	$recipe_categories = get_the_terms(get_the_ID(), 'recipe-category');
	$recipe_cuisines =  get_the_terms(get_the_ID(), 'meal-type');

	$cook_time = (($recipe_time_to_table_hours * 60 + $recipe_time_to_table_minutes) - ($recipe_prep_hours * 60 + $recipe_prep_minutes)) / 60;
	$cook_time_hours = floor($cook_time_hours);
	$cook_time_minutes = round(($cook_time - $cook_time_hours) * 60);

	if($cook_time_minutes == 60)
	{
		$cook_time_minutes = 0;
		$cook_time_hours += 1;
	}

	$recipe_ingredients = array();
	while ( have_rows('ingredients') ) {
		the_row();
		$currIngredient = get_sub_field('ingredient');
		array_push($recipe_ingredients, $currIngredient);
	}

	$recipe_instructions = array();
	while ( have_rows('recipe_instructions') ) {
		the_row();
		$curr_instruction = get_sub_field('instruction');
		array_push($recipe_instructions, 
		'{
			"@type": "HowToStep",
			"text": "' . $curr_instruction . '"
		 }');
	}

	$recipe_instructions = preg_replace("/\r|\n|\t/", "", $recipe_instructions);

	$type = '@type';
	$nutrition_facts->$type = 'NutritionInformation';

	while ( have_rows('nutrition_facts') ) {
		the_row();
		
		switch(get_sub_field('nutrition')) {
			case "Calories":
				$curr_nutritionfacts = "calories";
				break;
			case "Total Carbohydrate":
				$curr_nutritionfacts = "carbohydrateContent";
				break;
			case "Cholesterol":
				$curr_nutritionfacts = "cholesterolContent";
				break;
			case "Total Fat":
				$curr_nutritionfacts = "fatContent";
				break;
			case "Dietary Fiber":
				$curr_nutritionfacts = "fiberContent";
				break;
			case "Protein":
				$curr_nutritionfacts = "proteinContent";
				break;
			case "Sodium":
				$curr_nutritionfacts = "sodiumContent";
				break;
		}
		$curr_nutrition_amount = get_sub_field('nutrition_amount');
		$nutrition_facts->$curr_nutritionfacts = $curr_nutrition_amount;
	}

	$review_component = [];

	foreach ($reviews as &$value) {
		$entry = 
		'{
			"@type": "Review",
			"reviewRating": {
				"@type": "Rating",
				"bestRating": "5",
				"worstRating": "1",
				"ratingValue": "' . $value['Rating'] . '"
			},
			"reviewBody": "' . $value['ReviewText'] . '",
			"author": {
				"@type": "Person",
				"name": "' . $value['UserNickname'] . '"
			}
		}';	
		$entry = trim(preg_replace('/\s\s+/', ' ', $entry));
		array_push($review_component, $entry);
	}

	$recipe_cuisine = array();
	if(!empty($recipe_cuisines)) {
		foreach ($recipe_cuisines as &$cuisine) {
			array_push($recipe_cuisine, $cuisine->name);
		}
	}

	$recipe_category = array();
	if(!empty($recipe_categories)) {
		foreach ($recipe_categories as &$category) {
			array_push($recipe_category, $category->name);
		}
	}

	$video = array();

	if (have_rows('videos')) {
		while (have_rows('videos')) {
			the_row();
			if (get_sub_field('video')) {
				array_push($video, get_sub_field('video'));
			}
		}
	}

	if(!empty($video)) {
		$video_id = $video[0];
		$video_post = get_post($video_id);
		$video_thumbnail = wp_get_attachment_url(get_post_meta($video_id, "_uncode_poster_image", true));
	}

	$video_component = '';
	
	if($video_post->guid != '')
	{
		$video_component = 
		'"video": {
			"@type": "VideoObject",
			"name":"' . $video_post->post_title . '",
			"description":"' . $video_post->post_content . '",
			"thumbnailUrl": "' . $video_thumbnail . '",
			"uploadDate": "' . $video_post->post_date . '",
			"contentUrl": "' . $video_post->guid . '"
		},';
	}

	$category_component = (!empty($recipe_category)) ? '"recipeCategory": ' . json_encode($recipe_category) . ',': '';
	$cuisine_component = (!empty($recipe_cuisine)) ? '"recipeCuisine": ' . json_encode($recipe_cuisine) . ',' : '';

	echo 
		'<script type="application/ld+json">
			[
				{
					"@context": "http://schema.org",
					"@type": "Recipe",
					"mainEntityOfPage": "' . $recipe_url . '",
					"name": "' . $recipe_name . '",
					"image": ["' . $recipe_thumbnail_url . '" ],
					"recipeIngredient": ' . json_encode($recipe_ingredients) . ',
					"prepTime": "' . 'P0DT' . $recipe_prep_hours . 'H' . $recipe_prep_minutes . 'M' . '",
					"cookTime": "' . 'P0DT' . $cook_time_hours . 'H' . $cook_time_minutes . 'M' . '",
					"totalTime": "' . 'P0DT' . $recipe_time_to_table_hours . 'H' . $recipe_time_to_table_minutes . 'M' . '",
					"recipeYield": "' . $recipe_yield . '",' .
					$category_component . 
					$cuisine_component . 
					$video_component . ' 
					"nutrition": ' . json_encode($nutrition_facts) . ',
					"aggregateRating": {
						"@type": "AggregateRating",
						"itemReviewed": "' . $recipe_name . '",
						"ratingValue": "' . $rating_value . '",
						"reviewCount": "' . $rating_count . '" 
					},
					"review": ' . json_encode($review_component) . ', 
					"recipeInstructions": ' . json_encode($recipe_instructions) . '
				} 
			]
		 </script>';
?>

<meta itemprop="url" content=<?php echo home_url($wp->request)?> />
<?php
}?>
<!-- End Rich Pins -->
<?php 
if ( basename( $template ) === 'single-recipe.php' || basename( $template ) === 'single-holiday_recipe.php') 
{
	echo '	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>';
}
?>
<?php wp_head(); ?>

</head>
<?php
	global $LOGO, $metabox_data, $onepage, $fontsizes, $is_redirect, $menutype;

	if ($post_id !== null) {
		$metabox_data = get_post_custom($post_id);
		$metabox_data['post_id'] = $post_id;
	} else {
		$metabox_data = array();
	}

	$onepage = false;
	$background_div = $background_style = $background_color_css = '';

	if (isset($metabox_data['_uncode_page_scroll'][0]) && $metabox_data['_uncode_page_scroll'][0] == 'on') {
		$onepage = true;
	}

	$boxed = ot_get_option( '_uncode_boxed');
	$vmenu_position = ot_get_option('_uncode_vmenu_position');
	$fontsizes = ot_get_option( '_uncode_heading_font_sizes');
	$background = ot_get_option( '_uncode_body_background');

	if (isset($metabox_data['_uncode_specific_body_background'])) {
		$specific_background = unserialize($metabox_data['_uncode_specific_body_background'][0]);
		if ($specific_background['background-color'] != '' || $specific_background['background-image'] != '') {
			$background = $specific_background;
		}
	}

	$back_class = '';
	if (!empty($background) && ($background['background-color'] != '' || $background['background-image'] != '')) {
		if ($background['background-color'] !== '') {
			$background_color_css = ' style-'. $background['background-color'] . '-bg';
		}
		$back_result_array = uncode_get_back_html($background, '', '', '', '', 'div');

		if ((strpos($back_result_array['mime'], 'image') !== false)) {
			$background_style .= (strpos($back_result_array['back_url'], 'background-image') !== false) ? $back_result_array['back_url'] : 'background-image: url(' . $back_result_array['back_url'] . ');';
			if ( isset( $background['background-repeat'] ) && $background['background-repeat'] !== '' ) {
				$background_style .= 'background-repeat: '. $background['background-repeat'] . ';';
			}
			if ( isset( $background['background-position'] ) && $background['background-position'] !== '' ) {
				$background_style .= 'background-position: '. $background['background-position'] . ';';
			}
			if ( isset( $background['background-size'] ) && $background['background-size'] !== '' ) {
				$background_style .= 'background-size: '. ($background['background-attachment'] === 'fixed' ? 'cover' : $background['background-size']) . ';';
			}
			if ( isset( $background['background-attachment'] ) && $background['background-attachment'] !== '' ) {
				$background_style .= 'background-attachment: '. $background['background-attachment'] . ';';
			}
		} else {
			$background_div = $back_result_array['back_html'];
		}
		if ($background_style !== '') {
			$background_style = ' style="'.$background_style.'"';
		}
		if (isset($back_result_array['async_class']) && $back_result_array['async_class'] !== '') {
			$back_class = $back_result_array['async_class'];
			$background_style .= $back_result_array['async_data'];
		}
	}

	$body_attr = '';
	if ($boxed === 'on') {
		$boxed_width = ' limit-width';
	} else {
		$boxed_width = '';
		$body_border = ot_get_option('_uncode_body_border');
		if ($body_border !== '' && $body_border !== 0) {
			$body_attr = ' data-border="' . esc_attr($body_border) . '"';
		}
	}

	if ( uncode_is_full_page(true) ) {
		if ( isset($metabox_data['_uncode_scroll_additional_padding'][0]) && $metabox_data['_uncode_scroll_additional_padding'][0] != '' ) {
			$fp_add_padding = $metabox_data['_uncode_scroll_additional_padding'][0];
		} else {
			$fp_add_padding = 0;
		}

		$body_attr .= ' data-additional-padding="' . floatval($fp_add_padding) . '"';
	}


?>
<body <?php body_class($background_color_css); echo wp_kses_post( $body_attr ); ?>>
<!--[if lte IE 8]>
    <div class="ie-banner" id="IEBanner">
		<p>Your web browser (Internet Explorer 7) is out of date. Update your browser for more security, speed and the best experience on this site</p>
		<a href="https://www.whatismybrowser.com/guides/how-to-update-your-browser/auto" class="iebutton information">More Information</a>
	</div>
<![endif]-->
	<?php echo uncode_remove_p_tag( $background_div ) ; ?>
	<?php do_action( 'before' );

	$body_border = ot_get_option('_uncode_body_border');
	if ($body_border !== '' && $body_border !== 0) {
		$general_style = ot_get_option('_uncode_general_style');
		$body_border_color = ot_get_option('_uncode_body_border_color');
		if ($body_border_color === '') {
			$body_border_color = ' style-' . $general_style . '-bg';
		} else {
			$body_border_color = ' style-' . $body_border_color . '-bg';
		}
		$body_border_frame ='<div class="body-borders" data-border="'.$body_border.'"><div class="top-border body-border-shadow"></div><div class="right-border body-border-shadow"></div><div class="bottom-border body-border-shadow"></div><div class="left-border body-border-shadow"></div><div class="top-border'.$body_border_color.'"></div><div class="right-border'.$body_border_color.'"></div><div class="bottom-border'.$body_border_color.'"></div><div class="left-border'.$body_border_color.'"></div></div>';
		echo wp_kses_post( $body_border_frame );
	}

	?>
	<div class="box-wrapper<?php echo esc_html($back_class); ?>"<?php echo wp_kses_post($background_style); ?>>
		<div class="box-container<?php echo esc_attr($boxed_width); ?>">
		<script type="text/javascript">UNCODE.initBox();</script>
		<?php
			$remove_menu = (isset($metabox_data['_uncode_specific_menu_remove'][0]) && $metabox_data['_uncode_specific_menu_remove'][0] === 'on') ? true : false;
			if ( ! $remove_menu ) {
				if ($is_redirect !== true) {
					if ($menutype === 'vmenu-offcanvas' || $menutype === 'menu-overlay' || $menutype === 'menu-overlay-center') {
						$mainmenu = new unmenu('offcanvas_head', $menutype);
						echo uncode_remove_p_tag( $mainmenu->html );
					}
					if ( !($menutype === 'vmenu' && $vmenu_position === 'right') || (wp_is_mobile() && ($menutype === 'vmenu' && $vmenu_position === 'right') ) ) {
						$mainmenu = new unmenu($menutype, $menutype);
						echo uncode_remove_p_tag( $mainmenu->html );
					}
				}
			}
			?>
			<script type="text/javascript">UNCODE.fixMenuHeight();</script>
			<div class="basketfull-container" style="display:none;">
				<div class="basketfull-content">
					<span class="basketfull-close">✕</span>
					<div class="iframe-container">
						<iframe id="shopIframe" title="Iframe to store" data-src="https://shop.basketful.co/bundle?bundle=32f2f136-076b-411a-ae13-8cf5ee30fb2b" allowfullscreen>
						</iframe>
					</div>
				</div>
			</div>
			<div class="main-wrapper">
				<div class="main-container">
					<div class="page-wrapper<?php if ($onepage) { echo ' main-onepage'; } ?>">
						<div class="sections-container">
